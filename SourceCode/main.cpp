#include<string>
#include<iostream>
#include<algorithm>
#include<queue>
#include<functional>
#include<map>
#include<vector>
#include"generateRandom.h"
#include<time.h>
#include<sstream>
using namespace std;
clock_t startTime;
clock_t endTime;
string to_string(double val)
{
	stringstream stream;
	stream<<val;
	return stream.str();
}
double getNextItem(fstream&file)
{
	double result;
	int prePosition = file.tellg();
	if (file >> result)
	{
		file.seekg(prePosition);
		return result;
	}
	return 2140000000.0;
}
bool isEmptyFile(fstream&file)
{
	double t;
	int prePosition = file.tellg();
	if (file >> t)
	{
		file.seekg(prePosition);
		return true;
	}
	return false;
}
vector<double> getRun(fstream&file)
{
	vector<double> v;
	double d1 , d2;
	if (!(file>>d1))
	{
		return v;
	}
	v.push_back(d1);
	int prePosition =file.tellg();;
	while (file>>d2)
	{
		if (d2 >= d1)
		{
			v.push_back(d2);
			d1 = d2;
		}
		else
		{
			file.seekg(prePosition);
			break;
		}
		prePosition = file.tellg();
	}
	return v;
}
vector<vector<double>> getNRun(fstream*files, int n)
{
	vector<vector<double>> v;
	for (int i = 0; i < n; i++)
	{
		vector<double> run = getRun(files[i]);
		if (run.size() != 0)
		{
			v.push_back(run);
		}
	}
	return v;
}
bool existsAtLeastOneFile(fstream*files, int n)
{
	for (int i = 0; i < n; i++)
	{
		if (files[i])
		{
			return true;
		}
	}
	return false;
}
void openNFiles(fstream* files,int n, string prefix)
{
	for (int i = 0; i<n; i++)
	{
		files[i].open(to_string(i) + prefix + ".dat", ios::out |ios::in|ios::binary);
	}
}
void openNFilesToWrite(fstream* files, int n, string prefix)
{
	for (int i = 0; i<n; i++)
	{
		files[i].open(to_string(i) + prefix + ".dat", ios::out | ios::trunc | ios::binary);
	}
}
void closeNFiles(fstream* files, int n)
{
	for (int i = 0; i<n; i++)
	{
		files[i].close();
	}
}
void distribute(fstream& file,fstream*subFiles, int numberOfWay)
{
	double d1, d2;
	int index = 0;
	openNFilesToWrite(subFiles,numberOfWay,"left");
	file >> d1, subFiles[index] << d1 << " ";
	while (file>>d2)
	{
		if (d2 < d1)
		{
			index++;
		}
		subFiles[index%numberOfWay] << d2 << " ";
		d1 = d2;
	}
	closeNFiles(subFiles, numberOfWay);
}
bool oneRemainingFile(fstream*file, int n)
{
	int numberOfRemainingFile = 0;
	for (int i = 0; i < n; i++)
	{
		if (file[i])
		{
			numberOfRemainingFile++;
			if (numberOfRemainingFile>1)
			{
				return false;
			}
		}
	}
	return true;
}
void copyFile(fstream&source, fstream&dest)
{
	double temp;
	while (source >> temp)
	{
		dest << temp << " ";
	}
}
void merge(fstream&out,fstream*source, fstream*dest,int n, string mode)
{
	if (mode == "LeftToRight")
	{
		openNFiles(source, n, "left");
		openNFilesToWrite(dest, n, "right");
	}
	else  //mode == "RightToLeft"
	{
		openNFiles(source, n, "right");
		openNFilesToWrite(dest, n, "left");
	}
	int destFileIndex = 0;
	while (existsAtLeastOneFile(source, n))
	{
		/*vector<vector<double>> runs = getNRun(source,n);
		mergeRunAndWriteToFile(runs, dest[destFileIndex%n]);*/
		double*preValue = new double[n];
		priority_queue < double, vector < double >, greater < double >> minHeap;
		map<string, vector<int>> m;
		for (int i = 0; i < n; i++)
		{
			double trialItem = getNextItem(source[i]);
			if (trialItem != 2140000000.0)
			{
				minHeap.push(trialItem);
				m[to_string(trialItem)].push_back(i);
				source[i]>>preValue[i];
			}
		}
		while (minHeap.empty() == false)
		{
			double min = minHeap.top();
			minHeap.pop();
			vector<int> nextFiles = m[to_string(min)];
			for (int i = 0; i < m[to_string(min)].size(); i++)
			{
				dest[destFileIndex%n] << min << " ";
				int nextFile = m[to_string(min)][i];
				if (getNextItem(source[nextFile]) == 2140000000.0)
				{
					continue;
				}
				if (getNextItem(source[nextFile])>=preValue[nextFile])
			    {
					double temp;
					source[nextFile] >> temp;
					minHeap.push(temp);
					preValue[nextFile] = temp;
					m[to_string(temp)].push_back(nextFile);
				}
			}

		}
		destFileIndex++;
	}
	closeNFiles(dest, n);
	closeNFiles(source, n);
	if (destFileIndex == 1) //sorting done
	{
		string prefix = (mode == "LeftToRight") ? "right" : "left";
		dest[0].open("0" + prefix + ".dat", ios::in | ios::binary);
		endTime = clock();
		cout<<"Sorting done, Copying file\n==================================";
		copyFile(dest[0], out);
		out.close();
		return;
	}
	else
	{
		if (mode == "LeftToRight")
		{
            merge(out, dest, source, n, "RightToLeft");
		}
		else
		{
			merge(out, dest, source, n, "LeftToRight");
		}
	}
}

void sortKWay(string inputFileName, string outputFileName, int numberOfWay)
{
	//declare some file stream
	fstream inFile(inputFileName,ios::in|ios::binary);
	fstream outFile(outputFileName, ios::out| ios::binary);
	fstream* leftFiles = new fstream[numberOfWay];
	fstream*rightFiles = new fstream[numberOfWay];

	distribute(inFile,leftFiles, numberOfWay);
    merge(outFile,leftFiles,rightFiles,numberOfWay,"LeftToRight");

	inFile.close();
	outFile.close();
}
int main()
{
	string inputFileName = "in.dat";
	string outputFileName = "out.dat";
	int numberOfWay = 100;
	int numberOfRandom = 100000;
	generateRandom(inputFileName, numberOfRandom, 1.0, 100);
	cout << "RANDOM GENERATING DONE\n";
	startTime = clock();
	sortKWay(inputFileName, outputFileName,numberOfWay);
	double runTime = (endTime - startTime) / CLOCKS_PER_SEC;
	cout << "Run Time = " << runTime << endl;
	return 0;
}
