#include<fstream>
#include<ctime>
#include<cstdlib>
using namespace std;
double random(double min, double max)
{
	//srand(time(NULL));
	double f = ((double)rand()) / RAND_MAX;
	return min + f * (max - min);
}
//open a file by  fstream and write to the file
int generateRandom(string fileName, int length, double min, double max)
{
	fstream output(fileName, ios::out |ios::trunc| ios::binary);
	for (int i = 0; i < length; i++)
	{
		double randomNumber = random(min, max);
		output << randomNumber << " ";
	}
	output.close();
	return 0;
}
